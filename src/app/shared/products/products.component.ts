import {Component, EventEmitter, OnInit, Output} from '@angular/core';


declare var $: any;
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  productList = [];
  @Output() productModalClose: EventEmitter<void> = new EventEmitter<void>();
  constructor() { }

  ngOnInit() {

    this.productList = [
      {
        productId: '1',
        productName: 'Gooday Biscuit',
        productQty: '200gm',
        productPrice: '50',
      },
      {
        productId: '2',
        productName: 'Gooday Biscuit',
        productQty: '300gm',
        productPrice: '60',
      },
      {
        productId: '3',
        productName: 'Gooday Biscuit',
        productQty: '400gm',
        productPrice: '70',
      },
      {
        productId: '4',
        productName: 'Gooday Biscuit',
        productQty: '500gm',
        productPrice: '80',
      },
    ];
  }

  selectId(products) {
    localStorage.setItem('productId', products.productId);
    localStorage.setItem('productName', products.productName + ' ' + products.productQty);
    this.productModalClose.emit();
  }

}
