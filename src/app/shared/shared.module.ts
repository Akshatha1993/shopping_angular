import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { BottomBarComponent } from './bottom-bar/bottom-bar.component';
import { OptionListComponent } from './option-list/option-list.component';
import { ProductsComponent } from './products/products.component';


@NgModule({
  declarations: [HeaderComponent, BottomBarComponent, OptionListComponent, ProductsComponent],
  exports: [
    HeaderComponent,
    BottomBarComponent,
    OptionListComponent,
    ProductsComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
