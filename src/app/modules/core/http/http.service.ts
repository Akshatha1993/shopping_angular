import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DataService} from '../services/data.service';
import {Observable, throwError} from 'rxjs';
import {ObjectMapper} from 'json-object-mapper';
import {catchError} from 'rxjs/operators';
import {formatDate} from '@angular/common';
import {PROJECT_API_PATH, PROJECT_LOGIX_PATH, PROJECT_GETINITIALDATAS_PATH,
  PROJECT_SENDMESSAGE_PATH, PROJECT_CREATEGROUPMESSAGE_PATH, PROJECT_UPDATEREADSTATUS_PATH,
  PROJECT_GETGROUPDATAS_PATH, PROJECT_SENDATTRIBUTE_PATH, PROJECT_FETCHPOINTS_PATH,
  PROJECT_ID} from '../../../../environments/environment';
import {FormControl, Validators} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient,
              private dataService: DataService) {
  }


  public getGroupMessages(userid, parentid, fromlimit, tolimit, datatype, categories, userlist, searchkey) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const object = {
      user_id: userid,
      id: parentid,
      project_id: PROJECT_ID,
      from_limit: fromlimit,
      to_limit: tolimit,
      type: datatype,
      category: categories,
      contraList: '',
      userList: userlist,
      searchKey: searchkey
    };
    const stringObject: string = ObjectMapper.serialize(object) as string;

    return this.http
      .post(PROJECT_GETGROUPDATAS_PATH, stringObject, options)
      .pipe(catchError(this.handleError));
  }

  public sendMessage(object) {
    const options = {headers: {'Content-Type': 'application/json'}};
    const stringObject: string = ObjectMapper.serialize(object) as string;
    return this.http
      .post(PROJECT_SENDMESSAGE_PATH, stringObject, options)
      .pipe(catchError(this.handleError));
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    // window.alert(error.error.details);
    return throwError(error);
  }
}
