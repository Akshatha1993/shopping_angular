import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private refreshCommunication = new BehaviorSubject('');
  getrefreshCommunication = this.refreshCommunication.asObservable();

  private updateUserReadStatus = new BehaviorSubject('');
  getReadStatus = this.updateUserReadStatus.asObservable();

  private resObject = new BehaviorSubject('');
  getresponseObject = this.resObject.asObservable();

  private initiateCommunication = new BehaviorSubject('');
  getInitiatedCommunication = this.initiateCommunication.asObservable();

  private mapArray = new BehaviorSubject('');
  getMapArrayToMessage = this.mapArray.asObservable();

  private sentMesssage = new BehaviorSubject('');
  getsentMesssageStatus = this.sentMesssage.asObservable();

  private contraPartyObject = new BehaviorSubject('');
  getcontraPartyObject = this.contraPartyObject.asObservable();

  private answerMap = new BehaviorSubject('');
  getAnswerMap = this.answerMap.asObservable();

  constructor() { }

  setrefreshCommunication(obj: any) {
    this.refreshCommunication.next(obj);
  }

  setReadStatus(readStatusId: '') {
    this.updateUserReadStatus.next(readStatusId);
  }

  setMessageResObject(array: any) {
    this.resObject.next(array);
  }

  setInitiateCommunication(num: string) {
    this.initiateCommunication.next(num);
  }

  setMessages(array: any) {
    this.mapArray.next(array);
  }

  setContraPartyObject(obj: any) {
    this.contraPartyObject.next(obj);
  }

  setAnswerMap(obj: any) {
    this.answerMap.next(obj);
  }
}
