import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ShopListComponent } from './pages/shop-list/shop-list.component';
import {SharedModule} from '../../shared/shared.module';
import { HomeRoutingModule } from './home-routing.module';
import { ShopDetailComponent } from './pages/shop-detail/shop-detail.component';
import { ScrollBarComponent } from './pages/scroll-bar/scroll-bar.component';
import { AddressMapComponent } from './pages/address-map/address-map.component';
import { AgmCoreModule } from '@agm/core';
import {
  MatBottomSheetModule,
  MatListModule
} from '@angular/material';
import { ProductListComponent } from './pages/product-list/product-list.component';


@NgModule({
  declarations: [ShopListComponent, ShopDetailComponent, ScrollBarComponent, AddressMapComponent, ProductListComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    HomeRoutingModule,
    MatBottomSheetModule,
    MatListModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDeXOaEnaJerO_-unrwKQs9UyKoOxBE5Xw',
      libraries: ['places']
    }),
  ],
  entryComponents: [AddressMapComponent, ProductListComponent]
})
export class HomeModule { }
