
import {MAT_DATE_FORMATS, MatBottomSheetRef} from '@angular/material';
import {
  Component, ElementRef, EventEmitter, NgZone, OnInit
  , Output, ViewChild, AfterViewInit
} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MapsAPILoader, MouseEvent} from '@agm/core';
import {DataService} from '../../../core/services/data.service';

declare var google: any;
@Component({
  selector: 'app-address-map',
  templateUrl: './address-map.component.html',
  styleUrls: ['./address-map.component.scss']
})
export class AddressMapComponent implements AfterViewInit {

  @ViewChild('addressSearch', {static: true})
  private addressSearchElementRef: ElementRef;

  latitude;
  longitude;
  zoom;
  address;
  private geoCoder;

  constructor(private dataService: DataService,
              private bottomSheetRef: MatBottomSheetRef,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone) { }

  ngAfterViewInit() {

    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      const autocomplete = new google.maps.places.Autocomplete(this.addressSearchElementRef.nativeElement, {
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // get the place result
          const place = google.maps.places.PlaceResult = autocomplete.getPlace();
          // verify result
          this.address = (place.name + ',' + place.formatted_address);
          localStorage.setItem('kmpUserAddress', (place.name + ',' + place.formatted_address));
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          // set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          localStorage.setItem('kmpLattitude', this.latitude);
          localStorage.setItem('kmpLongitude', this.longitude);
          this.zoom = 12;
        });
      });
    });
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
        localStorage.setItem('kmpLattitude', this.latitude);
        localStorage.setItem('kmpLongitude', this.longitude);
      });
    }
  }

  // If you pass lat & long you will be getting address.
  getAddress(latitude, longitude) {
    this.geoCoder.geocode({location: {lat: latitude, lng: longitude}}, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
          localStorage.setItem('kmpUserAddress', results[0].formatted_address);
          // this.googleMapsAddressForm.controls.userAddress.setValue(results[0].formatted_address);
          // this.dataService.addUserAddress(results[0].formatted_address);
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }

  markerDragEnd($event: MouseEvent) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
    console.log(this.latitude, this.longitude);
    localStorage.setItem('kmpLattitude', this.latitude);
    localStorage.setItem('kmpLongitude', this.longitude);
  }


}
