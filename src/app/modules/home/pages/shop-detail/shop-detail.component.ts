import { Component, OnInit } from '@angular/core';
import {HttpService} from '../../../core/http/http.service';
import {DataService} from '../../../core/services/data.service';

@Component({
  selector: 'app-shop-detail',
  templateUrl: './shop-detail.component.html',
  styleUrls: ['./shop-detail.component.scss']
})
export class ShopDetailComponent implements OnInit {
  messageTitle;
  mapMessages;
  contraPartyObject = {
    contraPartyId: '',
    contraPartyName: ''
  };
  contraPartyId;
  contraPartyName;
  userid = localStorage.getItem('SH_UserID');

  constructor(private httpService: HttpService,
              private dataService: DataService) {

    this.dataService.getMapArrayToMessage.subscribe(response => {
      let messages;
      let mesTitle;
      messages = response;
      if (messages !== '') {
        this.mapMessages = messages.get('BOT').get(localStorage.getItem('SH_mesUniqueId'));
        for (let i = 0; i < this.mapMessages.length; i++) {
          let convertedDateTime;
          let time;
          time = this.mapMessages[i].message.time.slice(0, -3);
          convertedDateTime = this.time2TimeAgo(i, Number(time));
          this.mapMessages[i].message['timezone'] = convertedDateTime;
          if (this.mapMessages[i].message.message_type === 'QUESTION' && this.mapMessages[i].message.message.answer_map !== null) {
           this.dataService.setAnswerMap(this.mapMessages[i]);
          }
        }
        mesTitle = messages.get('BOT').get('0');
        for (let k = 0; k < mesTitle.length; k++) {
          if (mesTitle[k].unique_id === localStorage.getItem('SH_mesUniqueId')) {
            this.messageTitle = mesTitle[k].message.message.msg;
            if (localStorage.getItem('SH_UserID') === mesTitle[k].message.sender_id) {
              this.contraPartyId = mesTitle[k].message.receiver_id;
              this.contraPartyName = mesTitle[k].message.receiver_name;
            } else {
              this.contraPartyId = mesTitle[k].message.sender_id;
              this.contraPartyName = mesTitle[k].message.sender_name;
            }
          }
        }
        this.contraPartyObject.contraPartyId = this.contraPartyId;
        this.contraPartyObject.contraPartyName = this.contraPartyName;

        // this.dataService.setContraPartyObject(this.contraPartyObject);
      }
    });

  }

  ngOnInit() {
  }

  time2TimeAgo(i, time) {
    // This function computes the delta between the
    // provided timestamp and the current time, then test
    // the delta for predefined ranges.

    const d = new Date();  // Gets the current time
    const nowTs = Math.floor(d.getTime() / 1000); // getTime() returns milliseconds, and we need seconds, hence the Math.floor and division by 1000
    const seconds = nowTs - time;

    // morethan one year.
    if (seconds > 3.154e+7) {
      return Math.floor(seconds / 3.154e+7) + ' years ago';
    }

    // more that two days
    if (seconds > 2 * 24 * 3600) {
      return Math.floor(seconds / 86400) + ' days ago';
    }
    // a day
    // if (seconds > 24 * 3600) {
    //   console.log(Math.floor(seconds / 86400) + 'yesterday');
    //   return Math.floor(seconds / 86400) + ' yesterday';
    // }

    if (seconds > 3600) {
      return Math.floor(seconds / 3600) + ' hour ago';
    }
    // if (seconds > 1800) {
    //   console.log('Half an hour ago');
    // }
    if (seconds > 60) {
      return Math.floor(seconds / 60) + ' minute ago';
    }

    if (seconds < 60) {
      return  'Just now';
    }
  }
}
