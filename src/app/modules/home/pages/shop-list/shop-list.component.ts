import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {HttpService} from '../../../core/http/http.service';
import {DataService} from '../../../core/services/data.service';

@Component({
  selector: 'app-shop-list',
  templateUrl: './shop-list.component.html',
  styleUrls: ['./shop-list.component.scss']
})
export class ShopListComponent implements OnInit {

  array = [];
  shops = [];
  shopsArray;

  constructor(private router: Router,
              private httpService: HttpService,
              private dataService: DataService) {

    // this.dataService.getMapArrayToMessage.subscribe(response => {
    //   this.shops = [];
    //   this.shopsArray = response;
    //   // this.searchResult = false;
    //   if (this.shopsArray !== '') {
    //     let dataServiceMapArray;
    //       if (this.shopsArray.get('BOT').get('0') !== undefined) {
    //         dataServiceMapArray = this.shopsArray.get('BOT').get('0');
    //         for (let i = 0; i < dataServiceMapArray.length; i++) {
    //           if (dataServiceMapArray[i].message.category === 'BOT') {
    //             this.shops.push(dataServiceMapArray[i]);
    //           }
    //         }
    //         // for (let j = 0; j < this.questions.length; j++) {
    //         //   let convertedDateTime;
    //         //   let time;
    //         //   let convertedPoints;
    //         //   time = this.questions[j].message.time.slice(0, -3);
    //         //   convertedDateTime = this.time2TimeAgo(j, Number(time));
    //         //   this.questions[j].message['timezone'] = convertedDateTime;
    //         // }
    //       }
    //     }
    // });

  }

  ngOnInit() {
    this.shops = [
      {
        id : '1',
        msg: 'Families Super Market',
        desc: 'Families super market is in Garudacharpalya, Graphite India, Opposite to  metro station'
      }
    ];
  }

  toShopDetailScreen(shop) {
    localStorage.setItem('SH_mesUniqueId', shop.id);
    this.router.navigate(['/home/shopDetail']);
  }
}
