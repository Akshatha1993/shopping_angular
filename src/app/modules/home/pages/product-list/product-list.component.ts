import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  productList = [];

  constructor() { }

  ngOnInit() {

    this.productList = [
      {
        productId: '1',
        productName: 'Gooday Biscuit',
        productQty: '200gm',
        productPrice: '50',
      },
      {
        productId: '2',
        productName: 'Gooday Biscuit',
        productQty: '300gm',
        productPrice: '60',
      },
      {
        productId: '3',
        productName: 'Gooday Biscuit',
        productQty: '400gm',
        productPrice: '70',
      },
      {
        productId: '4',
        productName: 'Gooday Biscuit',
        productQty: '500gm',
        productPrice: '80',
      },
    ];

    if (localStorage.getItem('productId') !== null) {
      let productArray  = [];
      productArray = this.productList;
      for (let i = 0; i < productArray.length; i++) {
        if (productArray[i].productId === localStorage.getItem('productId')) {
          this.productList = [];
          this.productList.push(productArray[i]);
        }
      }
      localStorage.removeItem('productId');
      localStorage.removeItem('productName');
    }
  }


  increaseValue(i) {
    let value;
    value = parseInt((document.getElementById('number') as HTMLInputElement).value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    (document.getElementById('number') as HTMLInputElement).value = value;
  }

  decreaseValue(i) {
    let value;
    value = parseInt((document.getElementById('number') as HTMLInputElement).value, 10);
    value = isNaN(value) ? 0 : value;
    value < 1 ? value = 1 : '';
    value--;
    (document.getElementById('number') as HTMLInputElement).value = value;
  }
}
