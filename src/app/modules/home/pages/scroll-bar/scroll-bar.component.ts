import { Component, OnInit } from '@angular/core';
import {DataService} from '../../../core/services/data.service';
import * as sha1 from 'sha1/sha1';
import { PROJECT_ID } from '../../../../../environments/environment';
import {HttpService} from '../../../core/http/http.service';
import {MatBottomSheet} from '@angular/material';
import { AddressMapComponent } from '../address-map/address-map.component';
import { ProductListComponent } from '../product-list/product-list.component';

@Component({
  selector: 'app-scroll-bar',
  templateUrl: './scroll-bar.component.html',
  styleUrls: ['./scroll-bar.component.scss']
})
export class ScrollBarComponent implements OnInit {
  answerMapList = [];
  showOptionList = false;
  showProductList = false;
  productID;
  productName;

  constructor(private dataService: DataService,
              private httpService: HttpService,
              private bottomSheet: MatBottomSheet) {
    this.dataService.getAnswerMap.subscribe(response => {
      this.answerMapList = [];
      let map;
      let mymap;
      map = response;
      let key;
      if (map !== '') {
        this.answerMapList.push(map);

      }
    });
  }

  ngOnInit() {
  }
//CH_NGE
  answering(map, i) {
    let messageParent;
    messageParent = map.message.parent;
    let localCategory;
    localCategory = map.message.category;
    let responseObject;
    responseObject = [{
      unique_id: '',
      message: {
        parent: '',
        sender_points: '',
        unique_id: '',
        project_id: PROJECT_ID,
        receiver_id: map.message.sender_id,
        receiver_name: map.message.sender_name,
        sender_name: localStorage.getItem('SH_UserName'),
        sender_id: localStorage.getItem('SH_UserID'),
        message_type: 'ANSWER',
        message: {
          msg: '',
          answer_id: map.message.message.answer_map[i].id,
          org_question_id: map.message.message.question_id,
          answer_map: {}
        },
        receiver_type: 'I',
        time: '',
        category: 'CUSTOMER',
        attribute: {}

      },
      last_updated: ''
    }];
    let randomNum;
    let encryptedPassword;
    let stringTime;
    let hashedNum;
    let h;
    let unixTime;
    let timstamp;
    let object;
    let messagetext;
    if (map.message.message.answer_map[i].id === 'CH_NGE') {
      messagetext = 'Change your address';
      this.bottomSheet.open(AddressMapComponent);
    } else {
      messagetext = map.message.message.answer_map[i].lable;
    }
    randomNum = Math.floor(Math.random() * 90000) + 10000;
    encryptedPassword = sha1(localStorage.getItem('SH_UserID') + messagetext + randomNum);
    hashedNum = encryptedPassword.substring(0, 15);

    let result;
    let messageJson;
    responseObject[0].unique_id = (hashedNum);
    responseObject[0].message.message.msg = messagetext;
    responseObject[0].message.parent = messageParent;
    responseObject[0].message.unique_id = hashedNum;
    responseObject[0].message.sender_points = null;
    unixTime = new Date();
    timstamp = unixTime.getTime();
    h = timstamp;
    stringTime = h.toString();

    responseObject[0].message.time = stringTime;
    responseObject[0].last_updated = stringTime;
    messageJson = {
      msg: messagetext,
      answer_id: map.message.message.answer_map[i].id,
      org_question_id: map.message.message.question_id,
      answer_map: {}
    };

    object = [
      {
        unique_id: hashedNum,
        receiver_type: 'I',
        sender_name: localStorage.getItem('SH_UserName'),
        sender_id: localStorage.getItem('SH_UserID'),
        receiver_name: map.message.sender_name,
        receiver_id: map.message.sender_id,
        sender_points: null,
        message_type: 'ANSWER',
        message: messageJson,
        parent: messageParent,
        project_id: PROJECT_ID,
        random_key: randomNum,
        category: 'CUSTOMER',
        attribute: {
        }
      }
    ];
    this.dataService.setMessageResObject(responseObject);
    // this.showAnswerBar = !this.showAnswerBar;
    this.httpService.sendMessage(object).subscribe(response => {
      result = response;
      if (result.status === 'success') {
      }
    });
  }

  openOptions() {
    if ((document.getElementById('productinput') as HTMLInputElement).value !== '' && localStorage.getItem('productId') === null) {
      this.showProductList = true;
    } else if (localStorage.getItem('productId') !== null) {
      this.bottomSheet.open(ProductListComponent);
    } else {
      this.bottomSheet.open(ProductListComponent);
    }
  }

  openList() {
    this.showOptionList = true;
  }

  closeProductModal() {
    this.productID = localStorage.getItem('productId');
    this.productName = localStorage.getItem('productName');
    this.showProductList = false;
}

  closeOptionListModal() {
    this.showOptionList = false;
  }
}
