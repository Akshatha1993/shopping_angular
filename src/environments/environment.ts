// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const DOMAIN_NAME = 'http://backendless.io';
// export const DOMAIN_NAME = 'https://test-upfront-security.com/backendless_io';
// export const DOMAIN_NAME = 'https://app.knowtified.com/backendless_io';
export const PROJECT_PATH = DOMAIN_NAME + '/user/6705737436dbbc2bcb5c65d0f0042f9ed5d78233b826730fc8aa6ff42300f4e7';
export const PROJECT_LOGIX_PATH =  PROJECT_PATH + '/logix/cffc9d5178e0d47';
export const PROJECT_API_PATH =  PROJECT_PATH + '/api/cffc9d5178e0d47';
export const PROJECT_GETINITIALDATAS_PATH = 'https://myclasstor.com:7055/getInitialDatas';
export const PROJECT_SENDMESSAGE_PATH = 'https://myclasstor.com:7050/sentMessage';
export const PROJECT_UPDATEREADSTATUS_PATH = 'https://myclasstor.com:7058/UpdateReadStatus';
export const PROJECT_CREATEGROUPMESSAGE_PATH = 'https://myclasstor.com:7057/createGroup';
export const PROJECT_GETGROUPDATAS_PATH = 'https://myclasstor.com:7055/getGroupDatas';
export const PROJECT_CONNECT_SOCKET_PATH = 'https://myclasstor.com:7056/ws';
export const PROJECT_SENDATTRIBUTE_PATH = 'https://myclasstor.com:7050/sentAttributes';
export const PROJECT_FETCHPOINTS_PATH = 'https://myclasstor.com:7057/fetchPoints';

export const PROJECT_ID = '47f312';

export const environment = {
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
